package pl.edu.pwsztar.domain.checkers;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface MoveChecker {
    boolean checkKing(FigureMoveDto figureMoveDto);
    boolean checkQueen(FigureMoveDto figureMoveDto);
    boolean checkRock(FigureMoveDto figureMoveDto);
    boolean checkKnight(FigureMoveDto figureMoveDto);
    boolean checkPawn(FigureMoveDto figureMoveDto);
    boolean checkBishop(FigureMoveDto figureMoveDto);
}