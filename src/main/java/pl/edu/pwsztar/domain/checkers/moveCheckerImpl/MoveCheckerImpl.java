package pl.edu.pwsztar.domain.checkers.moveCheckerImpl;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.checkers.MoveChecker;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;

@Component
public class MoveCheckerImpl implements MoveChecker{

    @Override
    public boolean checkKing(FigureMoveDto figureMoveDto) {
        //TODO implement KING
        return false;
    }

    @Override
    public boolean checkQueen(FigureMoveDto figureMoveDto) {
        //TODO implement QUEEN
        return false;
    }

    @Override
    public boolean checkRock(FigureMoveDto figureMoveDto) {
        //TODO implement ROCK
        return false;
    }

    @Override
    public boolean checkKnight(FigureMoveDto figureMoveDto) {
        //TODO implement KNIGHT
        return false;
    }

    @Override
    public boolean checkPawn(FigureMoveDto figureMoveDto) {
        //TODO implement PAWN
        return false;
    }

    @Override
    public boolean checkBishop(FigureMoveDto figureMoveDto) {
        String start = figureMoveDto.getStart();
        String destination = figureMoveDto.getDestination();

        int startX = start.charAt(0);
        int startY = Integer.parseInt(start.charAt(2)+"");

        int destX = destination.charAt(0);
        int destY = Integer.parseInt(destination.charAt(2)+"");

        return Math.abs(startX - destX) == Math.abs(startY - destY);
    }
}



