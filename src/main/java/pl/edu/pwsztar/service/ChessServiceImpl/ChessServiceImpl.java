package pl.edu.pwsztar.service.ChessServiceImpl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.checkers.MoveChecker;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessServiceImpl.class);

    private final MoveChecker moveChecker;

    @Autowired
    ChessServiceImpl(MoveChecker moveChecker){
        this.moveChecker = moveChecker;
    }


    @Override
    public Boolean isMoveCorrect(FigureMoveDto figureMoveDto) {
        FigureType figureType = figureMoveDto.getType();
        boolean moveCorrect = false;

        LOGGER.info("Checking: \n"+figureMoveDto.toString());

        switch (figureType){
            case KING:
                moveCorrect = moveChecker.checkKing(figureMoveDto);
                break;
            case PAWN:
                moveCorrect = moveChecker.checkPawn(figureMoveDto);
                break;
            case ROCK:
                moveCorrect = moveChecker.checkRock(figureMoveDto);
                break;
            case QUEEN:
                moveCorrect = moveChecker.checkQueen(figureMoveDto);
                break;
            case KNIGHT:
                moveCorrect = moveChecker.checkKnight(figureMoveDto);
                break;
            case BISHOP:
                moveCorrect = moveChecker.checkBishop(figureMoveDto);
                break;
        }

        return moveCorrect;
    }
}
